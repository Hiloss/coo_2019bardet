package activeRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBconnection {

	private static DBconnection instance;
	private Connection connect;
	private String dbNom = "testPersonne";

	private DBconnection() {
		super();
		// variables a modifier en fonction de la base
		String userName = "root";
		String password = "";
		String serverName = "localhost";
		// Attention, sous MAMP, le port est 8889
		String portNumber = "3306";

		// iL faut une base nommee testPersonne !
		String dbName = dbNom;

		// creation de la connection
		Properties connectionProps = new Properties();
		connectionProps.put("user", userName);
		connectionProps.put("password", password);
		String urlDB = "jdbc:mysql://" + serverName + ":";
		urlDB += portNumber + "/" + dbName;
		try {
			connect = DriverManager.getConnection(urlDB, connectionProps);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Connection getConnect() {
		return connect;
	}

	public static synchronized DBconnection getInstance() {
		if (instance == null)
			instance = new DBconnection();
		return instance;
	}
	
	public static Connection getConnection() {
		return getInstance().connect;
	}
	
	public void setNomDB(String nomDB) throws SQLException {
		dbNom = nomDB;
		instance = null;
		getConnection();
	}
}
