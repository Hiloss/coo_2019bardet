package activeRecord;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PersonneTest {

	@Before
	public void initialiser() throws SQLException {
		Personne.createTable();
		Personne p1 = new Personne("Ouni", "Slim");
		p1.save();
		Personne p2 = new Personne("Guenego", "Pierre-Andr�");
		p2.save();
		Personne p3 = new Personne ("toto", "tata");
		p3.save();
	}
	
	@Test
	public void testFindAll() throws SQLException {
		//Pr�paration des donn�es
		ArrayList<Personne> list = new ArrayList<Personne>();
		
		//test de la m�thode
		list = Personne.findAll();
		
		//verification
		assertEquals(list.size(), 3);
	}
	
	@Test
	public void testfindById() throws SQLException {
		//Pr�paration des donn�es
		Personne p = null;
		Personne p1 = new Personne("Ouni", "Slim");
		p1.setId(1);
		
		//test de la m�thode
		p = Personne.findById(1);
		
		//verification
		assertEquals(p.getNom(), p1.getNom());
		assertEquals(p.getPrenom(), p1.getPrenom());
		assertEquals(p.getId(), p1.getId());
	}
	
	@Test
	public void testfindByName() throws SQLException {
		//Pr�paration des donn�es
		ArrayList<Personne> p = new ArrayList<Personne>();

		//test de la m�thode
		p = Personne.findByName("toto");
		
		//verification
		assertEquals(p.size(), 1);
	}

	@After
	public void finir() throws SQLException {
		Personne.deleteTable();
	}
}
