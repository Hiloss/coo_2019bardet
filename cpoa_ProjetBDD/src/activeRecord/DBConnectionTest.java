package activeRecord;

import static org.junit.Assert.*;

import java.sql.Connection;

import org.junit.Test;

public class DBConnectionTest {

	@Test
	public void testGetconnection() {
		//pr�paration de donn�e
		Connection c1;
		Connection c2;
		
		//executio de la mathode a tester
		c1 = DBconnection.getConnection();
		c2 = DBconnection.getConnection();
		
		//validation resultat
		assertEquals("C1 et C2 devait etre les m�mes connections", c1, c2);
	}

}
