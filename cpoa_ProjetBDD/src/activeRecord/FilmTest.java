package activeRecord;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author BARDET Brian
 * Classe de Test pour la classe Film
 */
public class FilmTest {

	@Before
	public void initialiser() throws SQLException, RealisateurAbsentException {
		//Cr�ation table personne
		Personne.createTable();
		Personne p1 = new Personne("Ouni", "Slim");
		p1.save();
		Personne p2 = new Personne("Guenego", "Pierre-Andr�");
		p2.save();
		Personne p3 = new Personne ("toto", "tata");
		p3.save();
		//Cr�ation table film
		Film.createTable();
		Film f1 = new Film("Alien", p1);
		f1.save();
		Film f2 = new Film("Blade Runner", p2);
		f2.save();
		Film f3 = new Film("Alien3", p3);
		f3.save();
	}
	
	/**
	 * Test de la m�thode findbyID
	 * @throws SQLException 
	 */
	@Test
	public void testFindById() throws SQLException {
		//Pr�paration des donn�es
		Film f = null;
		
		//test de la m�thode
		f = Film.findById(1);
		
		//verification
		assertEquals(f.getTitre(), "Alien");
		
	}

	/**
	 * Test de la m�thode getrealisateur
	 * @throws SQLException 
	 */
	@Test
	public void testGetRealisateur() throws SQLException {
		//Pr�paration des donn�es
		Personne p = null;
		Film f = Film.findById(1);
		Personne p1 = Personne.findById(1);
		
		//Test de la m�thode
		p = f.getRealisateur();
		
		//verification
		assertEquals(p.getNom(), p1.getNom());
		assertEquals(p.getPrenom(), p1.getPrenom());
		assertEquals(p.getId(), p1.getId());
	}

	@After
	public void finir() throws SQLException {
		Film.deleteTable();
		Personne.deleteTable();
	}
}
