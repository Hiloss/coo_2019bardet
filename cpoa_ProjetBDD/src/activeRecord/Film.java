package activeRecord;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author BARDET Brian
 * Classe qui repr�sente un film
 */
public class Film {

	/**
	 * Id du film
	 */
	private int id;
	/**
	 * ID du r�alisateur
	 */
	private int id_real;
	/**
	 * Titre du film
	 */
	private String titre;
	
	/**
	 * @param t Titre du film
	 * @param p Realsiateur de type Personne
	 */
	public Film(String t, Personne p) {
		id_real = p.getId();
		titre = t;
		id = -1;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_real() {
		return id_real;
	}

	public void setId_real(int id_real) {
		this.id_real = id_real;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * @param i Id film
	 * @param ir ID real
	 * @param t Titre
	 */
	private Film(int i, int ir, String t) {
		id = i;
		id_real = ir;
		titre = t;
	}
	
	/**
	 * retourne l'objet Film correspondant au tuple
	 * avec l'id pass� en param�tre.
	 * @param i  Index
	 * @return Film associ�
	 * @throws SQLException
	 */
	public static Film findById(int i) throws SQLException{
		String SQLPrep = "SELECT * FROM Film WHERE id=?;";
		PreparedStatement prep1 = DBconnection.getConnection().prepareStatement(SQLPrep);
		prep1.setString(1, Integer.toString(i));
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		// s'il y a un resultat
		Film res = null;
		if (rs.next()) {
			int idd = rs.getInt("id");
			String titre = rs.getString("titre");
			int id_rea = rs.getInt("id_rea");
			res = new Film(idd, id_rea, titre);
		}
		return res;
	}
	
	/**
	 * retourne l'ensemble
	 * des films r�alis� par la personne pass�e en param�tre.
	 * @param p Realisateur
	 * @return List de film realis� par la personne en param
	 * @throws SQLException
	 */
	public static ArrayList<Film> findByRealisateur(Personne p) throws SQLException{
		String SQLPrep = "SELECT * FROM Film WHERE id_rea=?;";
		PreparedStatement prep1 = DBconnection.getConnection().prepareStatement(SQLPrep);
		prep1.setInt(1, p.getId());
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		ArrayList<Film> res = new ArrayList<Film>();
		// s'il y a un resultat
		while (rs.next()) {
			int idd = rs.getInt("id");
			String titre = rs.getString("titre");
			int id_rea = rs.getInt("id_rea");
			res.add(new Film(idd, id_rea, titre));
		}
		return res;
	}
	
	/**
	 * retourne l'objet Personne correspondant
	 * au r�alisateur du film (en utilisant l'active record Personne et l'attribut id_real)
	 * @return Personne associ� a ce film
	 * @throws SQLException 
	 */
	public Personne getRealisateur() throws SQLException {
		return Personne.findById(id_real);
	}
	
	/**
	 * charge de cr�er la table film dans la bdd
	 * @throws SQLException 
	 */
	public static void createTable() throws SQLException {
		String createString = "CREATE TABLE Film ( id int(11) not null auto_increment,"
				+ " titre varchar(40) not null, id_rea int(11) default null, "
				+ "primary key (id), key id_rea (id_rea))";
		Statement stmt = DBconnection.getConnection().createStatement();
		stmt.executeUpdate(createString);
	}
	
	/**
	 * charge de supprimer la table Film dans la bdd
	 * @throws SQLException 
	 */
	public static void deleteTable() throws SQLException {
		String drop = "DROP TABLE Film";
		Statement stmt = DBconnection.getConnection().createStatement();
		stmt.executeUpdate(drop);
	}
	
	/**
	 *  ajoute le tuple correspondant � l'objet film
	 * this dans la base (cf 8.5). 
	 * @throws SQLException 
	 * @throws RealisateurAbsentException 
	 */
	public void save() throws SQLException, RealisateurAbsentException {
		if(id_real != -1) {
			if(id == -1) {
				saveNew();
			}else {
				update();
			}
		}else {
			throw new RealisateurAbsentException();
		}
	}
	
	/**
	 * pour ajouter dans la
	 * table
	 * @throws SQLException 
	 */
	public void saveNew() throws SQLException {
		String SQLPrep = "INSERT INTO Film (titre, id_rea) VALUES (?,?);";
		PreparedStatement prep;
		// l'option RETURN_GENERATED_KEYS permet de recuperer l'id (car
		// auto-increment)
		prep = DBconnection.getConnection().prepareStatement(SQLPrep, Statement.RETURN_GENERATED_KEYS);
		prep.setString(1, titre);
		prep.setInt(2, id_real);
		prep.executeUpdate();
		int autoInc = -1;
		ResultSet rs = prep.getGeneratedKeys();
		if(rs.next()) {
			autoInc = rs.getInt(1);
		}
		id = autoInc;
	}
	
	/**
	 * e pour mettre � jour le tuple existant
	 * @throws SQLException 
	 */
	public void update() throws SQLException {
		String SQLprep = "update Film set titre=?, id_rea=? where id=?;";
		PreparedStatement prep = DBconnection.getConnection().prepareStatement(SQLprep);
		prep.setString(1, titre);
		prep.setInt(2, id_real);
		prep.setInt(3, id);
		prep.execute();
	}
}
