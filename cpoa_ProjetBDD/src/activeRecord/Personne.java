package activeRecord;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Repr�sente une personne par son nom prenom id
 * @author BARDET Brian
 */
public class Personne {
	
	/**
	 * ID du personnage
	 */
	private int id;

	/**
	 * Nom et prenom de la personne
	 */
	private String nom, prenom;
	
	/**
	 * construit une Personne � partir
     * de son nom et de son pr�nom (id est initialis� � -1).
	 * @param n Nom
	 * @param p Prenom
	 */
	public Personne(String n, String p) {
		nom = n;
		prenom = p;
		id = -1;
	}
	
	/**
	 *  retourner l'ensemble des tuples de la classe
	 * Personne sous la forme d'objets
	 * @return L
	 * @throws SQLException 
	 */
	public static ArrayList<Personne> findAll() throws SQLException {
		String SQLPrep = "SELECT * FROM Personne;";
		PreparedStatement prep1 = DBconnection.getConnection().prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		ArrayList<Personne> list = new ArrayList<Personne>();
		// s'il y a un resultat
		while (rs.next()) {
			String nom = rs.getString("nom");
			String prenom = rs.getString("prenom");
			int id = rs.getInt("id");
			Personne p = new Personne(nom, prenom);
			p.id = id;
			list.add(p);
		}
		return list;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *i retourne la liste des objets Personne
	 * correspondant aux tuples dont le nom est pass� en param�tre.
	 * @return Liste de personne
	 * @throws SQLException 
	 */
	public static ArrayList<Personne> findByName(String n) throws SQLException {
		String SQLPrep = "SELECT * FROM Personne WHERE nom=?;";
		PreparedStatement prep1 = DBconnection.getConnection().prepareStatement(SQLPrep);
		prep1.setString(1, n);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		ArrayList<Personne> list = new ArrayList<Personne>();
		// s'il y a un resultat
		while (rs.next()) {
			String nom = rs.getString("nom");
			String prenom = rs.getString("prenom");
			int id = rs.getInt("id");
			Personne p = new Personne(nom, prenom);
			p.id = id;
			list.add(p);
		}
		return list;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/**
	 * retourne la liste des objets Personne
	 * correspondant aux tuples dont le nom est pass� en param�tre.
	 * @return personne
	 * @throws SQLException 
	 */
	public static Personne findById(int x) throws SQLException {
		String SQLPrep = "SELECT * FROM Personne WHERE id=?;";
		PreparedStatement prep1 = DBconnection.getConnection().prepareStatement(SQLPrep);
		prep1.setInt(1, x);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		Personne res = null;
		// s'il y a un resultat
		if (rs.next()) {
			String nom = rs.getString("nom");
			String prenom = rs.getString("prenom");
			int id = rs.getInt("id");
			res = new Personne(nom, prenom);
			res.id = id;
		}
		return res;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}
	
	/**
	 * charge de cr�er la table personne dans la bdd
	 * @throws SQLException 
	 */
	public static void createTable() throws SQLException {
		String createString = "CREATE TABLE Personne ( " + "ID INTEGER  AUTO_INCREMENT, "
				+ "NOM varchar(40) NOT NULL, " + "PRENOM varchar(40) NOT NULL, " + "PRIMARY KEY (ID))";
		Statement stmt = DBconnection.getConnection().createStatement();
		stmt.executeUpdate(createString);
	}
	
	/**
	 * charge de supprimer la table personen dans la bdd
	 * @throws SQLException 
	 */
	public static void deleteTable() throws SQLException {
		String drop = "DROP TABLE Personne";
		Statement stmt = DBconnection.getConnection().createStatement();
		stmt.executeUpdate(drop);
	}

	/**
	 *  ajoute le tuple correspondant � l'objet personne
	 * this dans la base (cf 8.5). 
	 * @throws SQLException 
	 */
	public void save() throws SQLException {
		if(id == -1) {
			saveNew();
		}else {
			update();
		}
	}
	
	/**
	 * pour ajouter dans la
	 * table
	 * @throws SQLException 
	 */
	public void saveNew() throws SQLException {
		String SQLPrep = "INSERT INTO Personne (nom, prenom) VALUES (?,?);";
		PreparedStatement prep;
		// l'option RETURN_GENERATED_KEYS permet de recuperer l'id (car
		// auto-increment)
		prep = DBconnection.getConnection().prepareStatement(SQLPrep, Statement.RETURN_GENERATED_KEYS);
		prep.setString(1, nom);
		prep.setString(2, prenom);
		prep.executeUpdate();
		int autoInc = -1;
		ResultSet rs = prep.getGeneratedKeys();
		if(rs.next()) {
			autoInc = rs.getInt(1);
		}
		id = autoInc;
	}
	
	/**
	 * e pour mettre � jour le tuple existant
	 * @throws SQLException 
	 */
	public void update() throws SQLException {
		String SQLprep = "update Personne set nom=?, prenom=? where id=?;";
		PreparedStatement prep = DBconnection.getConnection().prepareStatement(SQLprep);
		prep.setString(1, nom);
		prep.setString(2, prenom);
		prep.setInt(3, id);
		prep.execute();
	}
	
	/**
	 * charg�e de supprimer la
	 * personne sur laquelle la m�thode est appel�e. L'id de cette personne est ensuite remise
	 * � -1 puisqu'elle n'est plus pr�sente dans la tabl
	 * @throws SQLException 
	 */
	public void delete() throws SQLException {
		PreparedStatement prep = DBconnection.getConnection().prepareStatement("DELETE FROM Personne WHERE id=?");
		prep.setInt(1, id);
		prep.execute();
		id = -1;
	}
}
