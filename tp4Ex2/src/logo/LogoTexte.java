package logo;

public class LogoTexte extends DecorateurLogo{

	public LogoTexte(String d, Logo c) {
		super(d, 200, 200, 0.20, c);
	}
	
	@Override
	public MyImage getLogo(){
		MyImage i = composant.getLogo();
		i.textOver(nomIm, x, y);
        return i;
    }

}
