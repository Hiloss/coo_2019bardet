package logo;

public class DecorateurLogo extends Logo {

	protected Logo composant;
	protected int x,y;
	
	public DecorateurLogo(String d, int dx, int dy, double p, Logo c) {
		super(d,p);
		this.composant = c;
		this.nomIm = d;
		prix = p;
		x = dx;
		y = dy;
	}
	
	public String getNomIm() {
		return this.nomIm + composant.getNomIm();
	}
	
	public MyImage getLogo(){
		MyImage i = composant.getLogo();
		i.paintOver(nomIm, x, y);
        return i;
    }
	
	@Override
	public double combienCaCoute() {
		return this.prix + composant.combienCaCoute();
	}
}
