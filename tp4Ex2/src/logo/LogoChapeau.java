package logo;

public class LogoChapeau extends DecorateurLogo {

	public LogoChapeau(Logo c) {
		super("img/Chapeau.png", 280, 42, 0.20, c);
	}

}
