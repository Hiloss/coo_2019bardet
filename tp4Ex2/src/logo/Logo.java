package logo;

public abstract class Logo {

	/**
     * Chemin d'acces au fichier
     * contenant l'image de fond du logo
     */
    protected String nomIm;


	/**
     * Prix du logo
     */
    protected double prix;
    
    public Logo(String n, double p) {
    	nomIm = n;
    	prix = p;
    }
    
    /**
     * @return l'objet de MyImage correspondant a nomIm
     */
    public MyImage getLogo(){
        return new MyImage(nomIm);
    }
    
    /**
     * @return le prix du logo
     */
    public double combienCaCoute(){
        return prix;
    }
    
    /**
     * @return
     */
    public String getNomIm() {
		return nomIm;
	}
}
