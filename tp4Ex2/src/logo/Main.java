package logo;



public class Main
{
   
    public static void main(String args[])
    {
    	//Logo Rene
        Logo l = new ReneLaTaupe();
        LogoChapeau lc = new LogoChapeau(l);
        LogoLunette lcl = new LogoLunette(lc);
        LogoBaton lclb = new LogoBaton(lcl);
        LogoSmiley lclbs = new LogoSmiley(lclb);
        LogoTexte lT = new LogoTexte("UN TEXTE", lclbs);
        
        //Logo Crazy
        Logo l1 = new CrazyFrog();
        LogoChapeau lc1 = new LogoChapeau(l1);
        LogoLunette lcl1 = new LogoLunette(lc1);
        LogoBaton lclb1 = new LogoBaton(lcl1);
        LogoSmiley lclbs1 = new LogoSmiley(lclb1);
        LogoTexte lT1 = new LogoTexte("UN TEXTE", lclbs1);
        
        //Affichage Rene
        MyImage i = lT.getLogo(); 
        i.display();  // Permet l'affichage dans une fenetre de l'image associee
        
        System.out.println(lT.combienCaCoute());
        
        //Affichage Crazy
        MyImage i1 = lT1.getLogo();
        i1.display();
        
        System.out.println(lT1.combienCaCoute());
    }    
}
