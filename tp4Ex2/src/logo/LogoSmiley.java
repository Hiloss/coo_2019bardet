package logo;

public class LogoSmiley extends DecorateurLogo {

	public LogoSmiley(Logo c) {
		super("img/Smiley.png", 260, 210, 0.20, c);
	}

}
