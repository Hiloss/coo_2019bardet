package tp_note_2017;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author BARDET Brian
 *
 */
public class ControleurBouton implements ActionListener{

	private Modele model;
	
	public ControleurBouton(Modele m) {
		model = m;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Masquer"))
			model.setMasquer(!model.getMasquer());
	}
}
