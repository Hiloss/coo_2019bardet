package tp_note_2017;

/**
 * @author BARDET Brian
 * Rangee du mastermind
 */
public interface Rangee {

	public int[] getInt();
	
	public MyImage getImage(int n);
	
	public Modele getModele();
}
