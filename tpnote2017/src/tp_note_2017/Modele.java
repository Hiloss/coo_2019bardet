package tp_note_2017;

import java.util.Observable;


@SuppressWarnings("deprecation")
public class Modele extends Observable{

	/**
	 * est un bool�en indiquant si la combinaison � d�couvrir est masqu�e
	 * (true) ou non (false).
	 */
	private boolean masquer;
	/**
	 * est le num�ro de la rang�e courante
	 */
	private int ligneEnCours;
	/**
	 * est le num�ro de la case courante dans la rang�e courante
	 */
	private int colonneEnCours;
	/**
	 * est la rang�e, instance de RangeeSimple, contenant la combinaison
	 * de couleurs � d�couvrir (tir�es au hasard en d�but de partie)
	 */
	private Rangee secret;
	/**
	 * est une liste de Rangee, et contient initialement des instances de
	 * RangeeSimple, du jeu
	 */
	private Rangee[] rangees;
	private StrategyEval strategy;
	
	public Modele() {
		secret = null;
		masquer = false;
		ligneEnCours = 0;
		colonneEnCours = 0;
		
		//Cr�ation de la rang�e secr�te
		int[] intSecret = { (int) (Math.random() * 6 + 1), (int) (Math.random() * 6 + 1), (int) (Math.random() * 6 + 1),
				(int) (Math.random() * 6 + 1) };
		secret = new RangeeSimple(intSecret, this);
		
		rangees = new Rangee[8];
		
		for (int i = 0; i < rangees.length; i++) {
			int[] tab = {0,0,0,0};
			rangees[i] = new RangeeSimple(tab, this);
		}
	}

	/**
	 * modifie la couleur courante en fonction de l�entier
	 * pass� en param�tre et affecte cette couleur � la case courante de la rang�e en
	 * cours, puis incr�mente si besoin la colonne courante
	 * @param n 
	 */
	public void selectCouleur(int n) {
		Rangee r = rangees[ligneEnCours];
		r.getInt()[colonneEnCours] = n;
		setChanged();
		notifyObservers();
		System.out.println(colonneEnCours);
		setColonneEnCours(colonneEnCours+1);
	}
	
	/**
	 * v�rifie si les couleurs associ�es � chaque case de la ligne
	 * courante ont �t� choisies et, si c�est le cas, modifie la rang�e courante en lui
	 * ajoutant l��valuation de la rang�e
	 */
	public void valider() {
		
	}

	public int getColonneEnCours() {
		return colonneEnCours;
	}

	public void setColonneEnCours(int colonneEnCours) {
		if(colonneEnCours <4) {
			this.colonneEnCours = colonneEnCours;
			setChanged();
			notifyObservers();
		}
		else {
			valider();
		}
			
	}

	public boolean getMasquer() {
		return masquer;
	}
	
	public void setMasquer(boolean b) {
		masquer = b;
		System.out.println(masquer);
		setChanged();
		notifyObservers();
	}

	public int getLigneEnCours() {
		return ligneEnCours;
	}

	public Rangee getSecret() {
		return secret;
	}

	public Rangee[] getRangees() {
		return rangees;
	}

	public StrategyEval getStrategy() {
		return strategy;
	}

	public void setStrategy(StrategyEval strategy) {
		this.strategy = strategy;
	}
}
