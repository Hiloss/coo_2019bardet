package tp_note_2017;

public abstract class StrategyEval {

	public abstract int[] getResultat(Rangee r);
	
	public abstract String getNom();
	
}
