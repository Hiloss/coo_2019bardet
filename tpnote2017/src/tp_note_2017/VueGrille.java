package tp_note_2017;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

public class VueGrille extends JPanel implements Observer {

	private Modele model;
	private MyImage dessinGrille;
	public final static int TAILLE = 50;
	
	public VueGrille(ControleurCase c, Modele m) {
		super();
		model = m;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int[] ints = { 0, 0, 0, 0 }; 
		
		// dessine le fond en gris
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.BLACK);
		g.drawRect(7+TAILLE*model.getColonneEnCours(), this.getHeight()-TAILLE+8-(TAILLE*model.getLigneEnCours()), TAILLE-15, TAILLE-15);
		
		Rangee rangee;
		// on ajoute chacune des rangees en commencant l'affichage par le bas
		for (int i = 0; i < 8; i++) {
			rangee = model.getRangees()[i];
			// pour afficher par le bas, on part de la fin
			int hauteur = TAILLE*9 - (i + 1) * TAILLE;
			rangee.getImage(i).dessinerDansComposant(g, 0,hauteur);
		}

		// On ajoute la combinaison de couleurs tir�e au hasard � d�couvrir 
		// en haut du JPanel.
		if(!model.getMasquer()) {
			model.getSecret().getImage(-1).dessinerDansComposant(g, 0, 0);
		}
	}
	
	public void generer() {
		
	}
	@Override
	public void update(Observable o, Object arg) {
		repaint();
	}

	
}
