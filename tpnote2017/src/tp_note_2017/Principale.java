/**
 * 
 */
package tp_note_2017;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Classe � modifier selon l'enonce du TP, 
 * permet de construire l'interface graphique.
 *
 */
public class Principale {

	public static void main(String[] args) {		
				
		Modele m = new Modele();
		ControleurCase cc = new ControleurCase(m);
		
		// JPanel situ� au nord de l'IG contenant les rang�es
		// du jeu � remplir pour d�couvrir la bonne combinaison de couleurs
		VueGrille grille = new VueGrille(cc,m);
		grille.setPreferredSize(new Dimension(400,450));
		
		m.addObserver(grille);
		
		
		// JPanel au centre de l'IG contenant les 4 boutons
		JPanel panelBouton= new JPanel(new GridLayout(2,2));
		
		JButton jbv = new JButton("Valider");
		panelBouton.add(jbv);
		JButton jb = new JButton("Masquer");
		panelBouton.add(jb);
		ControleurBouton cb = new ControleurBouton(m);
		jb.addActionListener(cb);
		JButton btnExpert = new JButton("Expert");
		panelBouton.add(btnExpert);
		JButton btnDebutant = new JButton("Debutant");
		panelBouton.add(btnDebutant);
		
		
		// JPanel au sud de l'IG dans lequel se trouve l'affichage
		// des couleurs disponibles 
		ChoixCoul choixCoul= new ChoixCoul(m);
		ControleurCouleur controlcoul = new ControleurCouleur(m);
		choixCoul.addMouseListener(controlcoul);
		
		/*************************************
		 * Construction de l'IG dans une JFrame
		 *************************************/
		JFrame frame=new JFrame("Mastermind");	
		frame.add(grille,BorderLayout.NORTH); 
		frame.add(panelBouton,BorderLayout.CENTER);
		frame.add(choixCoul, BorderLayout.SOUTH);
		
		frame.pack();
		frame.setVisible(true);
	}

}
