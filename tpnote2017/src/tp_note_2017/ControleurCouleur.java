package tp_note_2017;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ControleurCouleur implements MouseListener{

	private Modele model;
	
	public ControleurCouleur(Modele m) {
		model = m;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		ChoixCoul c = (ChoixCoul)e.getSource();
		if(x>0 && x<300) {
			int couleurEnCours = x/50 +1;
			model.selectCouleur(couleurEnCours);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
