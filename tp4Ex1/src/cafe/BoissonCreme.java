package cafe;

/**
 * Classe permettant l'ajout de creme à une boisson 
 */
public class BoissonCreme extends DecorateurIngredient {
	
	/**
	 * Constructeur
	 * 
	 *  @param boisson à décorer
	 */
	public BoissonCreme(Boisson boisson) {
		super(0.55, " Creme", boisson); //prix = 0.55 description="Creme"
	}
	
	
}
