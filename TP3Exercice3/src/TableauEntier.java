import java.util.Iterator;

/**
 * @author BARDET Brian
 *
 */
public class TableauEntier implements Iterable<Integer>{

	private int[][] tab;
	private int largeur, hauteur;
	
	public TableauEntier(int[][] t) {
		tab = t;
		largeur = tab.length;
		hauteur = tab[0].length;
	}
	
	public int valeurA(int c, int l) {
		if(c < largeur && c >= 0 && l < hauteur && l >= 0)
			return tab[c][l];
		return -1;
	}
	
	public int getLargeur() {
		return largeur;
	}
	
	public int getHauteur() {
		return hauteur;
	}
	
	public ParcoursLigne iterateurLigne() {
		return new ParcoursLigne(this);
	}
	
	public ParcoursZigzag iterateurZigzag() {
		return new ParcoursZigzag(this);
	}
	
	public ParcoursColonne iterateurColonne() {
		return new ParcoursColonne(this);
	}

	@Override
	public Iterator<Integer> iterator() {
		return iterateurZigzag();
	}
}
