
/**
 * @author BARDET Brian
 *
 */
public class ParcoursLigne extends Parcours {

	public ParcoursLigne(TableauEntier t) {
		super(t);
	}

	@Override
	public void suivant() {
		if(colonneCour == tab.getLargeur()-1) {
			colonneCour = 0;
			ligneCour++;
		}else {
			colonneCour++;
		}
	}
}
