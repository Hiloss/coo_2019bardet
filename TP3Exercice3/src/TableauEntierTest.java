import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

/**
 * @author BARDET Brian
 *
 */
public class TableauEntierTest {

	@Test
	public void testIterateurLigne() {
		//Initialiser
		int[][] xy = {{1,3},{2,4}};
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(1);list.add(2);list.add(3);list.add(4);
		ArrayList<Integer> listTest = new ArrayList<Integer>();
		TableauEntier tab = new TableauEntier(xy);
		ParcoursLigne p = tab.iterateurLigne();
		while(p.hasNext()) {
			listTest.add(p.next());
		}
		assertEquals("Les deux listes devraient etre equals",list, listTest);
	}
	
	@Test
	public void testIterateurZigzag() {
		//Initialiser
		int[][] xy = {{1,3},{2,4}};
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(1);list.add(2);list.add(4);list.add(3);
		ArrayList<Integer> listTest = new ArrayList<Integer>();
		TableauEntier tab = new TableauEntier(xy);
		ParcoursZigzag p = tab.iterateurZigzag();
		while(p.hasNext()) {
			listTest.add(p.next());
		}
		assertEquals("Les deux listes devraient etre equals",list, listTest);
	}
	
	@Test
	public void testIterateurColonne() {
		//Initialiser
		int[][] xy = {{1,3},{2,4}};
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(1);list.add(3);list.add(2);list.add(4);
		ArrayList<Integer> listTest = new ArrayList<Integer>();
		TableauEntier tab = new TableauEntier(xy);
		ParcoursColonne p = tab.iterateurColonne();
		while(p.hasNext()) {
			listTest.add(p.next());
		}
		assertEquals("Les deux listes devraient etre equals",list, listTest);
	}
	
	@Test
	public void testIterator() {
		//Initialiser
		int[][] xy = {{1,3},{2,4}};
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(1);list.add(2);list.add(4);list.add(3);
		ArrayList<Integer> listTest = new ArrayList<Integer>();
		TableauEntier tab = new TableauEntier(xy);
		Iterator<Integer> p = tab.iterator();
		while(p.hasNext()) {
			listTest.add(p.next());
		}
		assertEquals("Les deux listes devraient etre equals",list, listTest);
	}

}
