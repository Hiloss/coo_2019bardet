
/**
 * @author BARDET Brian
 *
 */
public class ParcoursZigzag extends Parcours {

	public ParcoursZigzag(TableauEntier t) {
		super(t);
	}

	@Override
	public void suivant() {
		if(ligneCour%2 == 0) {
			if(colonneCour == tab.getLargeur()-1) {
				ligneCour++;
			}else 
				colonneCour++;
		}else {
			if(colonneCour == 0) 
				ligneCour++;
			else 
				colonneCour--;
			
		}
	}
}

