/**
 * @author BARDET Brian
 *
 */
public class ParcoursColonne extends Parcours {

	public ParcoursColonne(TableauEntier t) {
		super(t);
	}

	@Override
	public void suivant() {
		if (ligneCour == tab.getHauteur() - 1) {
			ligneCour = 0;
			colonneCour++;
		}else {
			ligneCour++;
		}
	}

}
