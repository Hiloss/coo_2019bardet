import java.util.Iterator;

/**
 * @author BARDET Brian
 *
 */
public abstract class Parcours implements Iterator<Integer>{

	protected TableauEntier tab;
	protected int ligneCour, colonneCour, nbParcourus;
	
	public Parcours(TableauEntier t) {
		tab = t;
		ligneCour = 0;
		colonneCour = 0;
		nbParcourus = 0;
	}
	
	@Override
	public boolean hasNext() {
		if (nbParcourus < tab.getHauteur()*tab.getLargeur())
			return true;
		return false;
	}

	@Override
	public Integer next() {
		int res = tab.valeurA(colonneCour, ligneCour);
		suivant();
		nbParcourus++;
		return res;
	}
	
	public abstract void suivant();
}
