
public class Principale {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Carre c;
		Rectangle r;
		AdaptateurTxt at;
		
		c = new Carre();
		r = new Rectangle();
		at = new AdaptateurTxt("Je suis un bête texte...");
		
		Dessinable[] t = {c,r,at};
		
		for (Dessinable d : t) {
			d.dessine();
		}
	}

}
