
public class AdaptateurTxt implements Dessinable {

	private TextDrawer adapte;
	private String txt;
	
	public AdaptateurTxt(String txt){
		this.txt=txt ;
		this.adapte= new TextDrawer();
	}
	
	@Override
	public void dessine() {
		// TODO Auto-generated method stub
		adapte.drawText(this.txt);
	}

}
