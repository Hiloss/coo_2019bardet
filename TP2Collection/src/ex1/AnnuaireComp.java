package ex1;

import java.util.HashMap;
import java.util.Set;

/**
 * @author BARDET Brian
 * Classe qui relie une personne a un num�ro
 */
@SuppressWarnings("serial")
public class AnnuaireComp extends HashMap<Personne, String>{
	
	/**
	 * Construit une Hasmap <Personne, String>
	 */
	public AnnuaireComp() {
		super();
	}
	
	/**
	 * affichage de l'ensemble des personnes (nom,prenom) de l'annuaire (appel� aussi le domaine de
	 * la table).
	 */
	public void domaine() {
		Set<Personne> domaine = keySet();
		for (Personne n : domaine)
			System.out.println(n.getNom() + " " + n.getPrenom());
	}
	
	/**
	 * acc�s au num�ro associ� au nom et au prenom donn� en param�tre; si ce nom et ce prenom n'existe pas, la
	 * m�thode retourne null.
	 * @param nom
	 * @return Num�ro  (null si nom n'existe pas)
	 */
	public String acces(String nom, String prenom) {
		String res = null;
		Personne p = new Personne(nom, prenom);
		if (containsKey(p))
			res = get(p);
		return res;
	}
	
	/**
	 *  adjonction d'un couple (nom prenom, num�ro) dans l'annuaire; le nom ne doit pas
	 *	d�j� �tre une entr�e de la table; il le devient apr�s l'adjonction; si nom prenom existe d�j�, la
	 *	m�thode ne fait rien
	 */
	public void adjonction(String nom, String prenom, String numero) {
		Personne p = new Personne(nom, prenom);
		if (! containsKey(p))
			put(p,numero);
	}
	
	/**
	 * suppression d'une entr�e de la table; si le nom n'existe pas, ne fait rien.
	 * @param nom
	 */
	public void suppresion(String nom, String prenom) {
		Personne p = new Personne(nom, prenom);
		if (containsKey(p))
			remove(p);
	}
	
	/**
	 * changement du num�ro associ� au nom donn� en param�tre; si nom
	 * n'existe pas, la m�thode ne fait rien.
	 * @param numero
	 */
	public void changement(String nom, String prenom, String numero) {
		Personne p = new Personne(nom, prenom);
		if (containsKey(p)) {
			remove(p);
			put(p, numero);
		}
	}
}
