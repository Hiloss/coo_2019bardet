package ex1;

/**
 * @author BARDET Brian
 *	Classe qui représente une Personne
 */
public class Personne {

	/**
	 * Nom et prenom de la personne
	 */
	private String nom, prenom;
	
	/**
	 * Construit une Personne
	 * @param n nom
	 * @param p prenom
	 */
	public Personne(String n, String p) {
		nom = n;
		prenom = p;
	}

	/**
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Change le nom de la personne
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Change le prenom de la personne
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		Personne p = (Personne)obj;
		if (p == null)
			return false;
		if (!p.getNom().equals(nom))
			return false;
		if (!p.getPrenom().equals(prenom))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}
}
