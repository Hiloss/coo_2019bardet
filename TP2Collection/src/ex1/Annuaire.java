package ex1;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Brian Bardet
 * Classe qui associe un nom � un num�ro
 */
@SuppressWarnings("serial")
public class Annuaire extends HashMap<String, String> {

	/**
	 * Constructeur de Annuaire
	 */
	public Annuaire() {
		super();
	}
	
	/**
	 * affichage de l'ensemble des noms de l'annuaire (appel� aussi le domaine de
	 * la table).
	 */
	public void domaine() {
		Set<String> domaine = keySet();
		for (String n : domaine)
			System.out.println(n);
	}
	
	/**
	 * acc�s au num�ro associ� au nom donn� en param�tre; si ce nom n'existe pas, la
	 * m�thode retourne null.
	 * @param nom
	 * @return Num�ro  (null si nom n'existe pas)
	 */
	public String acces(String nom) {
		String res = null;
		if (containsKey(nom))
			res = get(nom);
		return res;
	}
	
	/**
	 *  adjonction d'un couple (nom, num�ro) dans l'annuaire; le nom ne doit pas
	 *	d�j� �tre une entr�e de la table; il le devient apr�s l'adjonction; si nom existe d�j�, la
	 *	m�thode ne fait rien
	 */
	public void adjonction(String nom, String numero) {
		if (! containsKey(nom))
			put(nom,numero);
	}
	
	/**
	 * suppression d'une entr�e de la table; si le nom n'existe pas, ne fait rien.
	 * @param nom
	 */
	public void suppresion(String nom) {
		if (containsKey(nom))
			remove(nom);
	}
	
	/**
	 * changement du num�ro associ� au nom donn� en param�tre; si nom
	 * n'existe pas, la m�thode ne fait rien.
	 * @param numero
	 */
	public void changement(String nom, String numero) {
		if (containsKey(nom)) {
			remove(nom);
			put(nom, numero);
		}
	}
}
