package ex2;

public class AdresseIp implements Comparable<Object> {

	private String ip;
	
	public AdresseIp(String s) {
		ip = s;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public int compareTo(Object o) {
		int res = 0;
		AdresseIp anotherIp = (AdresseIp)o;
		String[] elemIp = ip.split("\\.");
		String[] elemAnIp = anotherIp.getIp().split("\\.");
		int i = 0; boolean stop = false;
		while (!stop & i < elemIp.length) {
			int e1 = Integer.parseInt(elemIp[i]);
			int e2 = Integer.parseInt(elemAnIp[i]);
			if(e1 < e2) {
				stop = true;
				res = -1;
			}else {
				if(e1 > e2) {
					stop = true;
					res = 1;
				}
			}
			i++;
		}
		return res;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdresseIp other = (AdresseIp) obj;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return ip;
	}
}
