package ex2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author BARDET Brian
 * Liste des IP presente dans le fichier logs
 */
public class ListeIp {
	
	/**
	 * Set d'ip
	 */
	private Set<AdresseIp> ips;
	
	/**
	 * Construit une ListeIp avec le nom du fichier
	 * @param name 
	 */
	public ListeIp(String name, boolean trier) {
		if(trier)
			ips = new TreeSet<AdresseIp>();
		else
			ips = new HashSet<AdresseIp>();
		try {
			chargerFichier(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Charger le fichier donn�e en param et modifie ips selon les ip contenu dans le fichier
	 * @param name
	 * @throws IOException 
	 */
	private void chargerFichier(String name) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader("logs.txt"));
		String ligne = in.readLine();
		while (ligne != null) {
			AdresseIp ip = new AdresseIp(ligne.split(" ")[0]);
			ips.add(ip);
			ligne = in.readLine();
		}
	}

	@Override
	public String toString() {
		return "ListeIp [ips=" + ips + "]";
	}
	
	public static void main(String[] args) {
		ListeIp liste = new ListeIp("logs.txt", false);
		System.out.println(liste);
	}
}
