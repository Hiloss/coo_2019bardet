package testEx1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ex1.Annuaire;

/**
 * @author Brian Bardet
 * Test de la classe Annuaire
 */
public class TestAnnuaire {

	/**
	 * Annuaire utiliser dans chaque m�thode de Test
	 */
	private Annuaire an;
	
	/**
	 * Initialisation de l'annuaire utiliser dans chaque m�thode de Test
	 */
	@Before
	public void initialiser() {
		an = new Annuaire();
	}
	
	/**
	 * Test de la fonction acces
	 */
	@Test
	public void testAcces() {
		//Pr�paration
		an.put("Herve", "0756543423");
		
		//Test
		String numero = an.acces("Herve");
		
		//V�rification
		assertEquals("Le num�ro devrait etre 0756543423", "0756543423", numero);
	}
	
	/**
	 * Test de la fonction adjonction
	 */
	@Test
	public void testAdjonction() {
		//Test
		an.adjonction("Herve", "0756543423");
		
		//V�rification
		assertEquals("Le num�ro devrait etre 0756543423", "0756543423", an.acces("Herve"));
	}
	
	/**
	 * Test de la fonction supression
	 */
	@Test
	public void testSupression() {
		//Pr�paration
		an.put("Herve", "0756543423");
		
		//Test
		an.suppresion("Herve");
		
		//V�rification
		assertEquals("Le cl� Herve ne devrait pas exister", null, an.acces("Herve"));
	}
	
	/**
	 * Test de la fonction changement
	 */
	@Test
	public void testChangement() {
		//Pr�paration
		an.put("Herve", "0756543423");
		
		//Test
		an.changement("Herve", "0756543424");
		
		//V�rification
		assertEquals("Le num�ro associ� devrait etre 0756543424", "0756543424", an.acces("Herve"));
	}

}
