package testEx1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ex1.AnnuaireComp;
import ex1.Personne;

/**
 * @author Brian Bardet
 * Test de la classe AnnuaireComp
 */
public class TestAnnuaireComp {

	/**
	 * Annuaire utiliser dans chaque m�thode de Test
	 */
	private AnnuaireComp an;
	/**
	 * Personne utiliser dans chaque m�thode de Test
	 */
	private Personne p;
	
	/**
	 * Initialisation de l'annuaire utiliser dans chaque m�thode de Test
	 */
	@Before
	public void initialiser() {
		an = new AnnuaireComp();
		p = new Personne("Bob","Grand");
	}
	
	/**
	 * Test de la fonction acces
	 */
	@Test
	public void testAcces() {
		//Pr�paration
		an.put(p, "0756543423");
		
		//Test
		String numero = an.acces("Bob","Grand");
		
		//V�rification
		assertEquals("Le num�ro devrait etre 0756543423", "0756543423", numero);
	}
	
	/**
	 * Test de la fonction adjonction
	 */
	@Test
	public void testAdjonction() {
		//Test
		an.adjonction("Bob","Grand", "0756543423");
		
		//V�rification
		assertEquals("Le num�ro devrait etre 0756543423", "0756543423", an.acces("Bob","Grand"));
	}
	
	/**
	 * Test de la fonction supression
	 */
	@Test
	public void testSupression() {
		//Pr�paration
		an.put(p, "0756543423");
		
		//Test
		an.suppresion("Bob","Grand");
		
		//V�rification
		assertEquals("Le cl� Herve ne devrait pas exister", null, an.acces("Bob","Grand"));
	}
	
	/**
	 * Test de la fonction changement
	 */
	@Test
	public void testChangement() {
		//Pr�paration
		an.put(p, "0756543423");
		
		//Test
		an.changement("Bob","Grand", "0756543424");
		
		//V�rification
		assertEquals("Le num�ro associ� devrait etre 0756543424", "0756543424", an.acces("Bob","Grand"));
	}

}
