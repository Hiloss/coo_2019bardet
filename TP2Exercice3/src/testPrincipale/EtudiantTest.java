package testPrincipale;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import principale.Etudiant;
import principale.Formation;
import principale.Identite;

/**
 * @author BARDET Brian
 *
 */
public class EtudiantTest {

	private Etudiant etu;
	
	@Before
	public void initialiser() {
		Identite i = new Identite("Bobo", "Toto", 1);
		Formation f = new Formation(1);
		f.addMatiere("Maths", 2);
		f.addMatiere("Fran�ais", 1);
		f.addMatiere("Informatique", 3);
		etu = new Etudiant(i, f);
	}
	
	/**
	 * Test de ajoutNote lorsque la mati�re n'existe pas mais est dans la formation
	 */
	@Test
	public void testAjoutNoteVide() {
		//Test
		etu.ajoutNote("Maths", 14);
		
		//Verification
		assertEquals("La taille de la map devrait etre de 1", 1, etu.getResultat().size());
		assertEquals("La taille la liste de note doit etre de 1", 1, etu.getResultat().get("Maths").size());
		assertEquals("La note devrait etre 14", 14, (int)etu.getResultat().get("Maths").get(0));
	}
	
	/**
	 * Test de ajoutNote lorsque la mati�re existe deja 
	 */
	@Test
	public void testAjoutNoteMatExiste() {
		//Test
		etu.ajoutNote("Maths", 14);
		etu.ajoutNote("Maths", 15);
		
		//Verification
		assertEquals("La taille de la map devrait etre de 1", 1, etu.getResultat().size());
		assertEquals("La taille la liste de note doit etre de 2", 2, etu.getResultat().get("Maths").size());
		assertEquals("La note devrait etre 15", 15, (int)etu.getResultat().get("Maths").get(1));
	}
	
	/**
	 * Test de ajoutNote lorsque la mati�re existe pas
	 */
	@Test
	public void testAjoutNoteMatExistepas() {
		//Test
		etu.ajoutNote("Geo", 14);
		
		//V�rification
		assertEquals("La taille de la map devrait etre de 0", 0, etu.getResultat().size());
	}
	
	/**
	 * Test de moyenneMat lorsque la matiere existe 
	 */
	@Test
	public void testmoyenneMatExiste() {
		//Test
		etu.ajoutNote("Maths", 14);
		etu.ajoutNote("Maths", 15);
		double res = etu.moyenneMat("Maths");
		
		//verification
		assertEquals(14.5, res, 0);
	}
	
	/**
	 * Test de moyenneMat lorsque la matiere existe pas
	 */
	@Test
	public void testmoyenneMatNonExiste() {
		//Test
		double res = etu.moyenneMat("Lala");
		
		//verification
		assertEquals(-1,res,0);
	}
}
