package principale;
/**
 * @author BARDET Brian
 *
 */
public class Identite {
	
	private String nom, prenom;
	private int nip;
	
	/**
	 * Construit une identit�
	 * @param n nom
	 * @param p prenom
	 * @param ni nip
	 */
	public Identite(String n, String p, int ni) {
		nom = n;
		prenom = p;
		nip = ni;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getNip() {
		return nip;
	}

	public void setNip(int nip) {
		this.nip = nip;
	}
}
