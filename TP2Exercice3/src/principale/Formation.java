package principale;
import java.util.HashMap;
import java.util.Map;

/**
 * @author BARDET Brian
 *
 */
public class Formation {

	private int id;
	private Map<String,Integer> matieres;

	/**
	 * Construit une formation sans mati�res
	 * @param i int
	 */
	public Formation(int i) {
		id = i;
		matieres = new HashMap<String,Integer>();
	}
	
	/**
	 * Ajoute une mati�re dans la map / si existe deja change le coefficiant
	 * @param s Nom de la mati�re
	 * @param coeff coefficiant
	 */
	public void addMatiere(String s, int coeff) {
		if(matieres.containsKey(s))
			matieres.remove(s);
		matieres.put(s,coeff);
	}
	
	/**
	 * Supprime une association dan la map / si existe pas alors ne fait rien
	 * @param s Nom de la mati�re
	 */
	public void removeMatiere(String s) {
		if(matieres.containsKey(s)) 
			matieres.remove(s);
	}
	
	/**
	 * Retourne le coeff d'une matiere donn�e (s)
	 * @param s String
	 * @return coeff / 0 si n'existe pas
	 */
	public int getCoeff(String s) {
		if(matieres.containsKey(s))
			return matieres.get(s);
		return 0;
	}

	/**
	 * @return Mati�res
	 */
	public Map<String, Integer> getMatieres() {
		return matieres;
	}
	
	
	
}
