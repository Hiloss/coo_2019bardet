package principale;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author BARDET Brian
 *
 */
public class Etudiant {
	
	private Map<String, ArrayList<Integer>> resultat;
	private Identite id;
	private Formation form;
	
	/**
	 * Construit un Etudiant
	 * @param i Identite
	 * @param f Formation
	 */
	public Etudiant(Identite i, Formation f) {
		id = i;
		form = f;
		resultat = new HashMap<String, ArrayList<Integer>>();
	}
	
	/**
	 * Associe une note a une matiere
	 * @param m Matiere
	 * @param n Note
	 */
	public void ajoutNote(String m, int n) {
		//La note doit etre compris entre 0 et 20 et la mati�re doit exister
		if(n>=0 && n<=20) {
			if(resultat.containsKey(m)) {
				ArrayList<Integer> list = resultat.get(m);
				list.add(n);
			}else {
				if(form.getMatieres().containsKey(m)) {
					resultat.put(m, new ArrayList<Integer>());
					ArrayList<Integer> list = resultat.get(m);
					list.add(n);
				}
			}
		}
	}
	
	/**
	 * Retourne la moyenne d'une matiere (m)
	 * @param m Mati�re
	 * @return double Moyenne / -1 si la mati�re existe pas
	 */
	public double moyenneMat(String m) {
		if(resultat.containsKey(m)) {
			ArrayList<Integer> list = resultat.get(m);
			int somme = 0;
			for(int i : list) {
				somme += i;
			}
			return (double)somme/list.size();
		}
		return -1;
	}
	
	public Map<String, ArrayList<Integer>> getResultat() {
		return resultat;
	}

	/**
	 * Retourne la moyenne g�n�rale de l'�tudiant
	 * @return double
	 */
	public double moyenneGen() {
		double res = 0;
		int sommeCoeff = 0;
		for(String n : resultat.keySet()) {
			double moyenneN = moyenneMat(n);
			int coeff = form.getCoeff(n);
			res += moyenneN*coeff;
			sommeCoeff+=coeff;
		}
		return (double)res/sommeCoeff;
	}
}
