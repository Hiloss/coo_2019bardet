package tp5ex2;

/**
 * @author BARDET Brian
 * Repr�sente un a�roport
 */
public class Aeroport {
	
	private boolean piste_libre;
	private static Aeroport instance;
	
	private Aeroport() {
		piste_libre = true;
	}
	
	public static synchronized Aeroport getInstance() {
		if (instance ==null) 
			instance = new Aeroport();
		return instance;
	}
	
	public synchronized boolean autoriserAdecoller() {
		if(piste_libre)
			return true;
		return false;
	}
	
	public synchronized boolean liberer_piste() {
		piste_libre = true;
		return true;
	}
}
