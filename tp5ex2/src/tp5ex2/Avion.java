package tp5ex2;

/**
 * @author BARDET Brian
 *
 */
public class Avion extends Thread {

	private String nom;
	private Aeroport a;

	public Avion(String s) {
		nom = s;
	}

	public void run() {
		a = Aeroport.getInstance();
		System.out.println("Je suis avion" + nom + " sur aeroport " + a);
		System.out.println("Je suis avion" + nom + " sur aeroport " + a + " et je d�colle");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		a.liberer_piste();
	}
}