package tp5ex1;

/**
 * @author BARDET Brian
 * Test la simulation de maniere visuelle
 */
public class MainSimulation {
	public static void main(String[] args) {
		FabriqueIntersection fi = new FabriqueIntersection();
		Simulateur s = new Simulateur(fi);
		s.genererStats();
		s.ecrireStats();
	}
}
