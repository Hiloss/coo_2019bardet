package tp5ex1;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @author BARDET Brian
 * Simule la fabrique de vehicule
 */
public class Simulateur {

	private FabriqueVehicule fv;
	private HashMap<String, Integer> proba;
	
	public Simulateur(FabriqueVehicule f) {
		fv = f;
	}
	
	/**
	 * @return Genere une map avec les stats de frabrication de vehicule
	 */
	public void genererStats() {
		HashMap<String, Integer> res = new HashMap<String, Integer>();
		int nbVoiture = 0, nbBus= 0 ,nbBicy= 0, nbPi = 0;
		for (int i = 0; i < 100; i++) {
			Vehicule v = fv.creerVehicule();
			switch (v.getType()) {
			case "Voiture":
				nbVoiture++;
				break;
			case "Bus":
				nbBus++;
				break;
			case "Bicyclette":
				nbBicy++;
				break;
			case "Pieton":
				nbPi++;
				break;
			}
		}
		res.put("Voiture", nbVoiture);
		res.put("Bus", nbBus);
		res.put("Bicyclette", nbBicy);
		res.put("Pieton", nbPi);
		proba = res;
	}
	
	/**
	 * Ecrit les stats de la creation de vehicule
	 */
	public void ecrireStats() {
		System.out.println(Arrays.asList(proba));
	}
}
