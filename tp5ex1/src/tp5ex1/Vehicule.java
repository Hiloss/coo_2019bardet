package tp5ex1;

/**
 * @author BARDET Brian
 * Repr�sente un v�hicule
 */
public abstract class Vehicule {

	private double vitesse, vitesseMax;
	private String type;
	
	/**
	 * @param v VitesseMax
	 * @param t Type
	 */
	public Vehicule(double v, String t) {
		vitesseMax = v;
		type = t;
	}

	/**
	 * @return vitesse
	 */
	public double getVitesse() {
		return vitesse;
	}

	/**
	 * @return Type du v�hicule
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * @param va augmentation vitesse
	 */
	public void accelerer(double va) {
		double vtempo = vitesse + va;
		if(vtempo <= vitesseMax)
			vitesse = vtempo;
	}
	
	/**
	 * @param va diminution vitesse
	 */
	public void decelerer(double va) {
		double vtempo = vitesse - va;
		if(vtempo <= 0)
			vitesse = vtempo;
	}

	@Override
	public String toString() {
		return "Vehicule [type=" + type + "]";
	}
}
