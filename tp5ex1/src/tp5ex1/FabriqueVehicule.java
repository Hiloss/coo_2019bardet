package tp5ex1;

/**
 * @author BARDET Brian
 * Interface pour fabriquer un v�hicule
 */
public interface FabriqueVehicule { 

	/**
	 * @return Un v�hicule
	 */
	public Vehicule creerVehicule();
}
