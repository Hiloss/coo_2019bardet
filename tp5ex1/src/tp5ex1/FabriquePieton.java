package tp5ex1;

/**
 * @author BARDET Brian
 * Fabrique des pietons (implements FabriqueVehicule)
 */
public class FabriquePieton implements FabriqueVehicule {

	/**
	 * @see tp5ex1.FabriqueVehicule#creerVehicule()
	 */
	@Override
	public Vehicule creerVehicule() {
		return new Pieton();
	}

}
