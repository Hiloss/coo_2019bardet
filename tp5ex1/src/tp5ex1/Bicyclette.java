package tp5ex1;

/**
 * @author BARDET Brian
 * Repr�sente un bicyclette qui est un v�hicule
 */
public class Bicyclette extends Vehicule {

	public Bicyclette() {
		super(25, "Bicyclette");
	}
}
