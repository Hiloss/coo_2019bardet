package tp5ex1;

/**
 * @author BARDET Brian
 * Repr�sente un pieton qui est un v�hicule
 */
public class Pieton extends Vehicule {

	public Pieton() {
		super(4, "Pieton");
	}
}
