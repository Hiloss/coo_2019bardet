package tp5ex1;

import java.util.TreeMap;

/**
 * @author BARDET Brian
 * Fabrique des véhicules selon une proba (implements FabriqueVehicule)
 */
public class FabriqueIntersection implements FabriqueVehicule {

	private double probaVoiture, probaBus, probaByc, probaPieton;
	
	/**
	 * Totale proba = 1
	 * @param v entre 0 et 1 proba voiture
	 * @param bus entre 0 et 1 proba bus
	 * @param byc entre 0 et 1 proba byc
	 * @param pie entre 0 et 1 proba pieton
	 */
	public FabriqueIntersection(double v, double bus, double byc, double pie) {
		if (v + bus + byc + pie == 1 && v >= 0 && bus >= 0 && byc >= 0 && pie >= 0) {
			probaVoiture = v;
			probaBus = bus;
			probaByc = byc;
			probaPieton = pie;
		}else {
			initialiser();
		}
	}
	
	public FabriqueIntersection() {
		initialiser();
	}
	
	private void initialiser() {
		probaVoiture = 0.8;
		probaBus = 0.05;
		probaByc = 0.05;
		probaPieton = 0.1;
	}
	
	/**
	 * @see tp5ex1.FabriqueVehicule#creerVehicule()
	 */
	@Override
	public Vehicule creerVehicule() {
		Vehicule res = null;
		double nb = Math.random();
		double int1 = probaVoiture;
		double int2 = int1 + probaBus;
		double int3 = int2 + probaByc;
		if(nb <= int1)
			res = new Voiture();
		else if(nb <= int2)
			res = new Bus();
			else if(nb <= int3)
				res = new Bicyclette();
				else 
					res = new Pieton();
		return res;
	}
}
