package tp5ex1;

/**
 * @author BARDET Brian
 * Fabrique des voitures (implements FabriqueVehicule)
 */
public class FabriqueVoiture implements FabriqueVehicule {

	/**
	 * @see tp5ex1.FabriqueVehicule#creerVehicule()
	 */
	@Override
	public Vehicule creerVehicule() {
		return new Voiture();
	}

}
