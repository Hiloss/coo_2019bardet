package ex2;

import java.util.ArrayList;
import java.util.Observable;

import javax.swing.JTextField;

@SuppressWarnings("deprecation")
public class Model extends Observable {

	private ArrayList<String> val;
	
	public Model() {
		val = new ArrayList<String>();
	}
	
	public void modifier(String t) {
		val.add(t);
		setChanged();
		notifyObservers();
	}

	public ArrayList<String> getVal() {
		return val;
	}
	
	
}
