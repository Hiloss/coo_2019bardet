package ex2;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

public class Controler implements KeyListener {

	private Model model;
	
	public Controler(Model m) {
		model = m;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		JTextField field = (JTextField)e.getSource();
		if(e.getKeyChar() == KeyEvent.VK_ENTER) {
			model.modifier(field.getText());
			field.setText("");
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
