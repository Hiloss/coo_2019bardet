package ex2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("deprecation")
public class Principale {

	
	public static void main(String[] args) { 
		 	
		 JTextField saisie; //Composants textuels de l'interface
		 VueGrandMot max;
		 VuePetitMot min;
		 VueCentre liste;

		 //JPanel Nord
		 saisie= new JTextField(10);
		 saisie.setPreferredSize(new Dimension(200,30));
		 JPanel panneauDeControle= new JPanel(new GridLayout(1,2));
		 panneauDeControle.add(new JLabel("Donner une cha�ne "+"    ",JLabel.CENTER));
		 panneauDeControle.add(saisie);
		 
		 Model m = new Model();
		 Controler c = new Controler(m);
		 saisie.addKeyListener(c);

		 //Zone centrale
		 liste = new VueCentre(5,5);
		 m.addObserver(liste);

		 //JPanel Sud
		 JPanel panMaxMin = new JPanel(new GridLayout(2,2));
		 panMaxMin.add(new JLabel("Plus grand mot ",JLabel.CENTER));
		 max= new VueGrandMot(10);
		 max.setPreferredSize(new Dimension(200,30));
		 panMaxMin.add(max);
		 
		 m.addObserver(max);

		 panMaxMin.add(new JLabel("Plus petit mot ", JLabel.CENTER));
		 min= new VuePetitMot(10);
		 panMaxMin.add(min);
		 
		 m.addObserver(min);

		 //Construction de l'IG dans une JFrame		 
		 JFrame frame=new JFrame();
		 frame.getContentPane().add(panneauDeControle,BorderLayout.NORTH);
		 frame.getContentPane().add(new JScrollPane(liste),BorderLayout.CENTER);
		 frame.getContentPane().add(panMaxMin,BorderLayout.SOUTH);
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setSize(new Dimension(400,400));
		 frame.setVisible(true);	
		
	}

} 


