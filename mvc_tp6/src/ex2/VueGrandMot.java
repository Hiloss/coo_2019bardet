package ex2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextField;

@SuppressWarnings("deprecation")
public class VueGrandMot extends JTextField implements Observer{

	public VueGrandMot(int x) {
		super(x);
		setText("");
	}
	@Override
	public void update(Observable o, Object arg) {
		ArrayList<String> mot = ((Model)o).getVal();
		Collections.sort(mot,new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o2.length()-o1.length();
			}
		});
		setText(mot.get(0));
	}
}
