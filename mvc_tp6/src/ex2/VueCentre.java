package ex2;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextArea;

@SuppressWarnings("deprecation")
public class VueCentre extends JTextArea implements Observer{

	public VueCentre(int x, int y) {
		super(x, y);
	}

	@Override
	public void update(Observable o, Object arg) {
		ArrayList<String> mot = ((Model)o).getVal();
		String res = "";
		for (int i = 0; i < mot.size() ; i++) {
			res += mot.get(i) + "\n";
		}
		setText(res);
	}
}
