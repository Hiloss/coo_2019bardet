package ex1;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("deprecation")
public class VueConsole implements Observer {

	public VueConsole() {
		System.out.println(0);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		System.out.println(((Model)o).getCompteur());
	}
}
